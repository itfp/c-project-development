# 2.3 C / C++ project management


The purpose of this talk is to present all the main aspects of the development
and the maintainance of a project based on C/C++.
We will mainly use C++, but all the tools we present should work also for C,
unless explicitely stated otherwise.

Feel free to reach out to us in case you encounter any error. 

Pull requests are welcome!

This talk has been divided into the follwing parts:

* Overview of the of the main tools and ideas behind project development and maintainance
* Overview of compilation, debugging and profiling tools
* Some examples of `cmake` usage.

Every part of the talk resides in his own subdirectory, with its own jupyter-notebook.
We originally wrote the content into jupyter-notebooks and not directly in
markdown because we presented the content as slides from the notebook using 
We will try and provide an updated text version of the notebook in case you do not have jupyter-notebook installed.

The topics presented here are very broad and presented briefly. 
We will add the references for further readings in case you are interested.

## prerequisites

If you wish to follow along this talk, we recommend that you install:


* gcc: which
* cmake: `sudo apt install cmake` on debian or ubuntu. ROOT requires cmake verison 3, which may be available as `cmake3` in derivatived of RHEL/CentOS such as Fedora.
* juppyter-notebooks : `sudo pip3 install jupyter` (`pip` or `pip3` according to your distribution)

## vscode

If you have vscode installed, we provide the configuration for the integration
of vscode with gdb for the example 03 of cmake. Your have to open in the same 
directory of this README.md, since this configuration depends on the directory
where vscode is opened into.
In particular, the file `.vscode/launch.json` the argument 
`"${workspaceFolder}/2-cmake/03-prng-lcg/build/ut_prngLcg"`
requires you to open vscode in this directory.
