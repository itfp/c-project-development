
# C/C++ project

The time required to maintain a small project in C/C++ can explode, since it is not interpreted and should be manually compiled. You should look a bit around and find the tools that allow you to reach the equilibrium between time spent managing the project and ease of upgradability. 

Main tools to develop C/C++ code

* compile system
* debugger
* profiler

(The editor is assumed for granted)

## Compiling

This is the first step.

You write source code, you want an executable. Sounds easy, but under the hoods it is an extremely complex process. 

The fact that the experience with these tools is so smooth is the consequence of the care that many truly experienced programmers put into designing and testing them. 

A world in which a compiler causes _segmentation faults_ as often as ROOT6 would be as horrible as Hell.

We are not going to cover all the details of what compiling means, you can gather more informations online.

https://www.slideshare.net/jserv/helloworld-internals

This covers what we are doing here: Basic example of compilation and static linking
* slide 17: basic example
* slide 29: static linking

It is not necessary to have an in-depth knowlodge of what a compiler does in order to use it.

### gcc

The first tool that you encounter when you start programming with C/C++ is `gcc`.

It is at the core of the GNU Project, it provides a compiler for C/C++ and other languages.

We will se some examples of its usage in the next section. We provide here only an overview. When using `gcc` you specify the source files, the directory with the headers, ecc.. It is a meticulous work providing all the correct flags and running the gcc commands in the correct order.

### make

In order not to repeat all the commands to build the project all the time, to encode the flow of the commands necessary to compile and in order to provide a cache not to recompile the whole project when only a small part is changed, `make` was created.

https://en.wikipedia.org/wiki/Make_(software)

We will not delve into how to write a proper makefile here. We just remind that C/C++ projects source code may be shipped with a `Makefile` in its the root directory and the only step required to compile the project is to run the command 

```
make
```

in the same directory where the `Makefiles` resides.

An example of a project that is shipped with a Makefile is the "simple terminal" of the "suckless tools" software collection, whose code is available at https://git.suckless.org/st/files.html

### GNU Build System - Autotools

https://en.wikipedia.org/wiki/GNU_Build_System

Since writing `Makefile`s manually may be a tedious work, tools have been developed to automatically write them from a configurration file.

Read https://autotools.io/index.html if you want more informations.

### cmake

cmake manages the build process of software. It is not limited to GNU/Linux (it can use whatever compiler you want under the hood, without you worrying).

cmake is an alternative to the GNU autotools. It started from the same needs and can also manage tests and packaging.

The official website is cmake.org and a very nice tutorial is available at https://cmake.org/cmake-tutorial/ (the code used there is available at https://gitlab.kitware.com/cmake/cmake/tree/master/Tests/Tutorial).

You may need some features of cmake that are not present into this tutorial, such as using installed cmake scripts to compile your code against ROOT/CERN or Geant4. We will cover those cases.

## Debugging

It does no matter how much care and attention you put when writing code, you will always encouter some troubles. 

Let technology help you, it may be faster than filling your code with `std::cout`s...

### gdb

This is the debugger that is usually shipped together with `gcc`. 

It can be easily integrated in your favourite editor/IDE, also `vscode`.

### valgrind

This is just an example of a profiler. The purpose is not to make an in-depth lesson on profiling, but to describe what they try to accomplish and why.
