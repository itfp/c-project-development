#ifndef PRNGLCG_H
#define PRNGLCG_H

typedef unsigned long long int randtype;

class prngLcg {
    private:
    randtype a;
    randtype c;
    // randtype m;
    randtype x;

    public:
    prngLcg();
    ~prngLcg();
    randtype randInt();
    double rand(double min=0., double max=1.); 
};

#endif //PRNGLCG_H