# 00 prng

Generate pseudo random numbers with `./ut_prngLcg`. 

Use `./ut_prngLcg 134` if you want 134 random numbers.

## Compile

`./compile.sh` : compile with a single command
`./compile.sh` : separate the compilation into multiple commands