#!/bin/bash

rm -rf build
mkdir build
cd build
cmake ..
# cmake .. -DCMAKE_BUILD_TYPE=Debug -DCMAKE_VERBOSE_MAKEFILE=ON
make -j4
cd ..