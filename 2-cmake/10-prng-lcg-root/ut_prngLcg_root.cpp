#include <iostream>
#include <string>
#include <bitset>
#include "prngLcg.h"

#include "TCanvas.h"
#include "TH1F.h"

int main(int numArg, char* listArg[]) {
    
    int N = 100000;
    if (numArg > 1){
        N = std::stoi( listArg[1] );
    }

    double xmin = -10;
    double xmax = 10;
    TH1F* histo = new TH1F("histo", "Uniformity of the Linear Congruential Generator", 100, xmin, xmax);
    prngLcg generator;
    for (int i=0; i<N; i++){
        histo->Fill(generator.rand(xmin, xmax));
    }
    TCanvas* c1 = new TCanvas("c1", "prng lcg", 700, 700);
    histo->SetMinimum(0.);
    histo->SetFillColor(kYellow);
    histo->Draw();
    c1->Print("prng-lcg.png", "png");

    return 0;
}