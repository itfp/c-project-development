cmake_minimum_required(VERSION 2.6)
project(simple_main)

# same old story
include_directories(include) # -I./include
add_subdirectory(prng) # prng/prngLcg.cpp

add_executable(ut_prngLcg ut_prngLcg.cpp)
target_link_libraries(ut_prngLcg prngLcg)

# program that integrates ROOT
list(APPEND CMAKE_PREFIX_PATH $ENV{ROOTSYS})
find_package(ROOT REQUIRED)
include(${ROOT_USE_FILE})

add_executable(ut_prngLcg_root ut_prngLcg_root.cpp)
target_link_libraries(ut_prngLcg_root ${ROOT_LIBRARIES} prngLcg)