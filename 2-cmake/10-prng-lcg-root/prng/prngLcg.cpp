#include "prngLcg.h"
#include <climits>

prngLcg::prngLcg(){
    a = 6364136223846793005U;
    c = 1442695040888963407U;
    // m = 18446744073709551616U;
    x = 1;
}

prngLcg::~prngLcg(){}

randtype prngLcg::randInt(){
    // x = (a*x + c) % m;
    x = ( a * x + c );
    return x;
}

double prngLcg::rand(double min, double max){
    return (double) (max - min) * ( (double) randInt() ) / 
        ( (double) ULLONG_MAX ) + min ;
}