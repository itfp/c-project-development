cmake_minimum_required(VERSION 2.6)
project(simple_main)

include_directories(include) # -I./include
add_subdirectory(prng) # prng/prngLcg.cpp

add_executable(ut_prngLcg ut_prngLcg.cpp)
target_link_libraries(ut_prngLcg prngLcg)